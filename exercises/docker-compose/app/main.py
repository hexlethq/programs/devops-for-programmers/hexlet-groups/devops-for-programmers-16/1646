from typing import Optional

from fastapi import FastAPI
import psycopg2

app = FastAPI()
conn = psycopg2.connect(
    dbname='hexlet', user='hexlet', 
    password='mypassword', host='database',
)



@app.get("/")
def read_root():
    cursor = conn.cursor()
    cursor.execute('SELECT COUNT(*) FROM users')
    user_count = cursor.fetchone()
    return {"greeting": "Hello Hexlet", "users count": user_count[0]}
